UPDATE film
SET rental_duration = 21, rental_rate = 9.99
WHERE title = 'Stardust';

UPDATE customers
SET firstname = (SELECT firstname FROM address WHERE name = 'Ilkhom'),
    lastname = (SELECT lastname FROM address WHERE name = 'Ilkhom'),
	create_date = current_date
WHERE id IN (
    SELECT customer_id
    FROM rentals
    GROUP BY customer_id
    HAVING COUNT(*) >= 10
) AND id IN (
    SELECT customer_id
    FROM payments
    GROUP BY customer_id
    HAVING COUNT(*) >= 10
);
